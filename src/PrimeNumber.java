import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {
        int num,count=0;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter number");
        num=scanner.nextInt();

        for(int i=2;i<=num/2;i++)
        {
            if(num%i==0)
            {   count++;
                break;
            }
        }
        if(count==0 && num!=1)
            System.out.println(num + " is a Prime number");
        else
            System.out.println(num + " is not a Prime number");

    }
}
